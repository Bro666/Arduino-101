# Arduino-101
[Arduino 101](https://www.arduino.cc/en/Main/ArduinoBoard101) experiments for [my article](http://www.raspberry-pi-geek.com/Archive/2016/16/Exploring-the-new-Arduino-Genuino-101) in [Raspberry Pi Geek](http://www.raspberry-pi-geek.com/) magazine number [16](http://www.raspberry-pi-geek.com/Archive/2016/16).

In this experiment, I use the gyroscope on the 101 to control the rotation a model 3D helicopter rendered using [Panda3d](https://www.panda3d.org/). You can see how this works (SPOILER ALERT: quite well) in [a video I made](https://youtu.be/s0LroSYF_j8).


The original [helicopter mesh](http://tf3dm.com/3d-model/a-low-poly-helicopter-35242.html) is the work of the very talented [sielxm3d](http://tf3dm.com/user/sielxm3d) and can be used for non-commercial work.
