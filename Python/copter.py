from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from panda3d.core import *

import serial
 
class MyWindow(ShowBase):
    def __init__(self):
        self.ser = serial. Serial('/dev/ttyACM0', 9600)

        ShowBase.__init__(self)
        
        base.disableMouse()
        
        alight = AmbientLight('alight')
        alight.setColor(Vec4(0.4, 0.4, 0.4, 1))
        alightNP = render.attachNewNode(alight)
        render.setLight(alightNP)
        
        plight = PointLight('plight')
        plight.setColor(VBase4(1, 1, 1, 1))
        plNP = render.attachNewNode(plight)
        plNP.setPos(10, 10, 0)
        render.setLight(plNP)

        self.copter = self.loader.loadModel("0000_MyModels/helicopter.egg")
        self.copter.setScale(1, 1, 1)
        self.copter.setPos(0, 20, 0)
        self.copter.setHpr(0, 0, 0);
        self.copter.reparentTo(self.render)
        
        self.taskMgr.add(self.rotate_copter, "Rotate Copter")        
    
    def get_YPR(self):
        self.Hpr=[int(i) for i in self.ser.readline().split()]
    
    def rotate_copter(self, task):
        self.get_YPR()
        self.copter.setHpr(self.Hpr[0],self.Hpr[1],self.Hpr[2]);
        return Task.cont
 
if __name__ == '__main__':
    win = MyWindow()
    win.run()
