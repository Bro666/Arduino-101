#include "CurieImu.h"
#include "MadgwickAHRS.h"

Madgwick filter;

int gx, gy, gz;
int yaw;
int pitch;
int roll;
int factor = 800; // variable by which to divide gyroscope values, used to control sensitivity
// note that an increased baud rate requires an increase in value of factor


void setup() {
  Serial.begin(9600);

  // initialize device
  CurieImu.initialize();

  // verify connection
  if (!CurieImu.testConnection()) {
    Serial.println("CurieImu connection failed");
  }

  // use the code below to calibrate gyro offset values
  //IMU device must be resting in a horizontal position for the following calibration procedure to work correctly!

  Serial.print("Starting Gyroscope calibration...");
  CurieImu.autoCalibrateGyroOffset();
  Serial.println(" Done");

  Serial.println("Enabling Gyroscope offset compensation");
  CurieImu.setGyroOffsetEnabled(true);  
}

void loop() {
  gx = CurieImu.getRotationX();
  gy = CurieImu.getRotationY();
  gz = CurieImu.getRotationZ();

  filter.updateIMU(gx/factor, gy/factor, gz/factor, 0.0, 0.0, 0.0);

  // functions to find yaw roll and pitch from quaternions
  yaw = int(filter.getYaw()*100);
  roll = int(filter.getRoll()*100);
  pitch = int(filter.getPitch()*100);


  Serial.print(yaw); Serial.print(" ");
  Serial.print(roll);Serial.print(" ");
  Serial.println(pitch);
  delay(10);
}
